import { Subscription } from 'rxjs';
import { RecipeDetailInfo } from 'src/app/shared/models/recipeDetailInfo';
import { RecipeSimilar } from 'src/app/shared/models/recipeSimilar';
import { DataService } from 'src/app/shared/services/data.service';
import { SpoonacularServiceService } from 'src/app/shared/services/spoonacular-service.service';

import { Component, OnInit } from '@angular/core';

import { LocalStorage } from '../../shared/helpers/common';

@Component({
  selector: 'app-recipe-detail-page',
  templateUrl: './recipe-detail-page.component.html',
  styleUrls: ['./recipe-detail-page.component.scss'],
})
export class RecipeDetailPageComponent implements OnInit {
  public screen(width) {
    return width < 700 ? 'sm' : 'lg';
  }
  // tslint:disable-next-line: no-object-literal-type-assertion
  public recipeDetailInfo: RecipeDetailInfo = {} as RecipeDetailInfo;
  // tslint:disable-next-line: no-object-literal-type-assertion
  public recipeFavoriteInfo: RecipeDetailInfo = {} as RecipeDetailInfo;
  // tslint:disable-next-line: no-object-literal-type-assertion
  public recipeSimilar: RecipeSimilar = {} as RecipeSimilar;
  public recipeImage = [];
  public isPopupVisible = false;
  public isRecipeFavorite = false;

  private subscriptions: Subscription[] = [];
  constructor(
    private dataService: DataService,
    private spoonacularService: SpoonacularServiceService
  ) {}

  ngOnInit(): void {
    this.subscriptions.push(
      this.dataService.recipeDataShare$.subscribe((result) => {
        if (result !== undefined && result !== null) {
          this.isRecipeFavorite = LocalStorage.checkRecipeExistById(result);

          this.subscriptions.push(
            this.spoonacularService
              .getDetailRecipeInfo(result.id)
              .subscribe((response: any) => {
                this.recipeDetailInfo = response;
                this.recipeImage = [this.recipeDetailInfo.image];
              })
          );
        }
      })
    );
  }

  public recipeSimilarOnClick(event) {
    try {
      this.subscriptions.push(
        this.spoonacularService
          .getSimilarRecipe(this.recipeDetailInfo.id)
          .subscribe((result: any) => {
            this.isPopupVisible = true;
            this.recipeSimilar = result;
          })
      );
    } catch (error) {
      throw error;
    }
  }

  public hiding(event) {
    try {
      this.isPopupVisible = false;
    } catch (error) {
      throw error;
    }
  }

  public buttonCloseClick(event) {
    try {
      this.isPopupVisible = false;
    } catch (error) {
      throw error;
    }
  }

  public recipeRemoveFromFavoriteOnClick(event) {
    try {
      if (this.recipeDetailInfo !== null) {
        LocalStorage.removeFromFavorites(this.recipeDetailInfo);
        this.isRecipeFavorite = LocalStorage.checkRecipeExistById(
          this.recipeDetailInfo
        );
      }
    } catch (error) {
      throw error;
    }
  }

  public recipeAddToFavoriteOnClick(event) {
    try {
      if (this.recipeDetailInfo !== null) {
        this.recipeFavoriteInfo.title = this.recipeDetailInfo.title;
        this.recipeFavoriteInfo.readyInMinutes = this.recipeDetailInfo.readyInMinutes;
        this.recipeFavoriteInfo.id = this.recipeDetailInfo.id;
        LocalStorage.addToFavorites(this.recipeFavoriteInfo);
        this.isRecipeFavorite = LocalStorage.checkRecipeExistById(
          this.recipeFavoriteInfo
        );
      }
    } catch (error) {
      throw error;
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
}

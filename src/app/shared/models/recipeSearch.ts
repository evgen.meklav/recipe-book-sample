export interface RecipeSearch {
  id: 0;
  title: '';
  imageType: '';
  favorite: boolean;
}

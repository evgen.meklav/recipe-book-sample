import { DxModule } from 'src/app/shared/dx.module';

import { HttpClientModule } from '@angular/common/http';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { RecipeSearchComponent } from './recipe-search.component';

describe('RecipeSearchComponent', () => {
  let component: RecipeSearchComponent;
  let fixture: ComponentFixture<RecipeSearchComponent>;
  let de: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientModule, DxModule],
      declarations: [RecipeSearchComponent],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(RecipeSearchComponent);
        component = fixture.componentInstance;
        de = fixture.debugElement.query(By.css('dx-form'));
      });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('dx-button on initial should be disabled', () => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;

    const btnSearch = compiled.querySelector('dx-button[id="idDetail"]');

    expect(btnSearch.classList.contains('dx-state-disabled')).toBeTruthy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecipeDetailPageComponent } from './recipe-detail-page/recipe-detail-page.component';
import { RecipeFavoritePageComponent } from './recipe-favorite-page/recipe-favorite-page.component';
import { RecipeSearchPageComponent } from './recipe-search-page/recipe-search-page.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'recipe-search',
    pathMatch: 'full',
  },
  { path: 'recipe-search', component: RecipeSearchPageComponent },
  { path: 'recipe-detail', component: RecipeDetailPageComponent },
  { path: 'recipe-favorite', component: RecipeFavoritePageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RecipeSearchPageRoutingModule {}

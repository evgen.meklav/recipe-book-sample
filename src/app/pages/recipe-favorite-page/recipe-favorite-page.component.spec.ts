import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeFavoritePageComponent } from './recipe-favorite-page.component';

describe('RecipeFavoritePageComponent', () => {
  let component: RecipeFavoritePageComponent;
  let fixture: ComponentFixture<RecipeFavoritePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RecipeFavoritePageComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeFavoritePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('text should be List of favorites recipes', () => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;

    const text = compiled.querySelector('b');

    expect(text.textContent).toContain('List of favorites recipes');
  });
});

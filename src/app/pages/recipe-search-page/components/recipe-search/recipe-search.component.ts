import { DataService } from './../../../../shared/services/data.service';
import { RecipeDetailInfo } from './../../../../shared/models/recipeDetailInfo';
import { RecipeSearch } from './../../../../shared/models/recipeSearch';
import { ComplexSearchRecipe } from './../../../../shared/models/complexSearchRecipe';
import { SpoonacularServiceService } from './../../../../shared/services/spoonacular-service.service';
import { Component, OnInit } from '@angular/core';
import { OtherUtilities, LocalStorage } from 'src/app/shared/helpers/common';
import CustomStore from 'devextreme/data/custom_store';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recipe-search',
  templateUrl: './recipe-search.component.html',
  styleUrls: ['./recipe-search.component.scss'],
})
export class RecipeSearchComponent implements OnInit {
  public selectedItem: any = {};
  public recipeSearch: RecipeSearch[];

  public searchModeOption: string = 'contains';
  public searchExprOption: any = 'title';

  public detailRecipeDisabled: boolean = true;

  public recipeFavoriteList = [];

  public selectedRecipeText: string;

  private subscriptions: Subscription[] = [];

  public screen(width) {
    return width < 700 ? 'sm' : 'lg';
  }

  constructor(
    private spoonacularService: SpoonacularServiceService,
    private dataService: DataService
  ) {
    this.selectedItem = {};
  }

  ngOnInit(): void {
    try {
      // tslint:disable-next-line: no-object-literal-type-assertion
      this.recipeFavoriteList = LocalStorage.getFromFavorites();
      this.selectedRecipeText = '';
    } catch (error) {
      throw error;
    }
  }

  public onValueChangedRecipe(event) {
    // tslint:disable-next-line: no-object-literal-type-assertion
    const params: ComplexSearchRecipe = {} as ComplexSearchRecipe;
    if (this.selectedItem === null || this.selectedItem === undefined) {
      this.detailRecipeDisabled = true;
    }
    if (
      event.value !== undefined &&
      event.value !== null &&
      event.value.length >= 2
    ) {
      params.query = event.value;
      params.number = 10;
      this.subscriptions.push(
        this.spoonacularService
          .getAutoCompleteRecipeSearch(OtherUtilities.toHttpParams(params))
          .subscribe((response: any) => {
            response.map((result) => {
              result.favorite = false;
            });
            this.recipeSearch = OtherUtilities.checkFavoriteRecipe(
              response,
              this.recipeFavoriteList
            );
          })
      );
    }
  }

  public onItemClick(event) {
    if (this.selectedItem !== undefined && this.selectedItem !== null) {
      this.detailRecipeDisabled = false;
      if (this.selectedItem.favorite) {
        this.selectedRecipeText = 'Your favorite recipe, successfuly selected!';
      } else {
        this.selectedRecipeText = 'Recipe, successfuly selected!';
      }
    } else {
      this.detailRecipeDisabled = true;
    }
  }

  public recipeDetailPage(event) {
    try {
      if (this.selectedItem !== undefined && this.selectedItem != null) {
        this.dataService.nextNavigationPage('recipe-detail');
        this.dataService.setRecipeDataShare(this.selectedItem);
      }
    } catch (error) {
      throw error;
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }
}

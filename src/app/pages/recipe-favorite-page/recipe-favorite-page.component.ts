import { Component, OnInit } from '@angular/core';
import { LocalStorage } from 'src/app/shared/helpers/common';

@Component({
  selector: 'app-recipe-favorite-page',
  templateUrl: './recipe-favorite-page.component.html',
  styleUrls: ['./recipe-favorite-page.component.scss'],
})
export class RecipeFavoritePageComponent implements OnInit {
  public recipeFavorite = [];
  constructor() {
    //
  }

  ngOnInit(): void {
    this.recipeFavorite = LocalStorage.getFromFavorites();
  }
}

import notify from 'devextreme/ui/notify';

import { ErrorHandler, Injectable } from '@angular/core';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  public handleError(error) {
    notify(`${error.message}: ${error.statusText}`, 'error');
    // throw error;
  }
}

import { DEFAULT_TIMEOUT } from './interceptors/timeout.interceptor';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RecipeSearchPageComponent } from './pages/recipe-search-page/recipe-search-page.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import {
  HeaderInterceptor,
  ResponseInterceptor,
  GlobalErrorHandler,
  TimeoutInterceptor,
} from './interceptors';
import { RecipeSearchComponent } from './pages/recipe-search-page/components/recipe-search/recipe-search.component';

import {
  DxDrawerModule,
  DxTreeViewModule,
  DxScrollViewModule,
  DxToolbarModule,
  DxSelectBoxModule,
  DxListModule,
  DxAutocompleteModule,
  DxLoadIndicatorModule,
  DxBoxModule,
  DxGalleryModule,
  DxButtonModule,
  DxPopupModule,
  DxDataGridModule,
  DxResponsiveBoxModule,
} from 'devextreme-angular';
import {
  HeaderComponent,
  SideNavOuterToolbarComponent,
} from './shared/components';
import { RecipeSearchPageRoutingModule } from './pages/recipe-search-page-routing.module';
import { RecipeDetailPageComponent } from './pages/recipe-detail-page/recipe-detail-page.component';
import { RecipeFavoritePageComponent } from './pages/recipe-favorite-page/recipe-favorite-page.component';
import { DxoLabelModule } from 'devextreme-angular/ui/nested';

@NgModule({
  declarations: [
    AppComponent,
    RecipeSearchPageComponent,
    RecipeSearchComponent,

    HeaderComponent,
    SideNavOuterToolbarComponent,

    RecipeDetailPageComponent,
    RecipeFavoritePageComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    DxTreeViewModule,
    DxDrawerModule,
    DxScrollViewModule,
    DxToolbarModule,
    DxSelectBoxModule,
    DxListModule,
    RecipeSearchPageRoutingModule,
    DxAutocompleteModule,
    DxLoadIndicatorModule,
    DxBoxModule,
    DxoLabelModule,
    DxGalleryModule,
    DxButtonModule,
    DxPopupModule,
    DxDataGridModule,
    DxResponsiveBoxModule,
  ],
  providers: [
    [{ provide: HTTP_INTERCEPTORS, useClass: TimeoutInterceptor, multi: true }],
    [{ provide: DEFAULT_TIMEOUT, useValue: 10000 }],
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HeaderInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ResponseInterceptor,
      multi: true,
    },
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

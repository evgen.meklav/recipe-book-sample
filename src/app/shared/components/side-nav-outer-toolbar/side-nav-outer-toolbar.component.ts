import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { DxScrollViewComponent } from 'devextreme-angular/ui/scroll-view';
import { Subscription } from 'rxjs';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-side-nav-outer-toolbar',
  templateUrl: './side-nav-outer-toolbar.component.html',
  styleUrls: ['./side-nav-outer-toolbar.component.scss'],
})
export class SideNavOuterToolbarComponent implements OnInit {
  @ViewChild(DxScrollViewComponent, { static: true })
  public scrollView: DxScrollViewComponent;

  public menuOpened: boolean;
  public selectedRoute = '';
  public title = '';

  public navigation = [];

  private subscriptions: Subscription[] = [];

  constructor(private router: Router, private dataService: DataService) {
    this.navigation = [
      {
        text: 'Recipe search',
        icon: '',
        path: 'recipe-search',
      },
      {
        text: 'Recipe detail',
        icon: '',
        path: 'recipe-detail',
      },
      {
        text: 'Recipe favorite',
        icon: '',
        path: 'recipe-favorite',
      },
    ];
  }

  public ngOnInit() {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.selectedRoute = val.urlAfterRedirects.split('?')[0];

        if (this.selectedRoute === '/recipe-search') {
          this.title = 'Recipe search';
        }
        if (this.selectedRoute === '/recipe-detail') {
          this.title = 'Recipe detail';
        }
        if (this.selectedRoute === '/recipe-favorite') {
          this.title = 'Recipe favorite';
        }
      }
    });
    this.subscriptions.push(
      this.dataService.navigationPage$.subscribe((path) => {
        if (path !== undefined && path !== null) {
          this.router.navigate([path]);
        }
      })
    );
  }

  public navigationChanged(event) {
    const path = event.itemData.path;
    const pointerEvent = event.event;
    if (path && this.menuOpened) {
      this.router.navigate([path]);
      this.scrollView.instance.scrollTo(0);
      this.menuOpened = false;
      pointerEvent.stopPropagation();
    } else {
      pointerEvent?.preventDefault();
    }
  }
}

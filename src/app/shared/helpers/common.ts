import { RecipeSearch } from './../models/recipeSearch';
import { HttpParams } from '@angular/common/http';
import notify from 'devextreme/ui/notify';

export const OtherUtilities = {
  // tslint:disable-next-line: ban-types
  toHttpParams(obj: Object): HttpParams {
    return Object.getOwnPropertyNames(obj).reduce(
      (p, key) => p.set(key, obj[key]),
      new HttpParams()
    );
  },
  checkFavoriteRecipe(
    recipeSearch: RecipeSearch[],
    recipeFavoriteList: any[]
  ): RecipeSearch[] {
    try {
      let x: number = 0;
      recipeSearch.forEach((recipeList) => {
        let isRecipeFavorite = false;
        recipeFavoriteList.forEach((recipeFavList) => {
          if (recipeList.id === recipeFavList.id) {
            isRecipeFavorite = true;
          }
        });
        if (isRecipeFavorite) {
          recipeSearch[x].favorite = true;
        } else {
          recipeSearch[x].favorite = false;
        }
        x += 1;
      });

      return recipeSearch;
    } catch (error) {
      throw error;
    }
  },
};

export const LocalStorage = {
  addToFavorites(recipe: any): void {
    try {
      let recipeFavorites = [];
      if (localStorage.getItem('favorites')) {
        recipeFavorites = JSON.parse(localStorage.getItem('favorites'));
        let recipeIdExist = false;
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < recipeFavorites.length; i++) {
          if (recipeFavorites[i].id === recipe.id) {
            recipeIdExist = true;
            notify('Already your favorite recipe', 'success', 3000);
            break;
          }
        }
        if (!recipeIdExist) {
          recipeFavorites.push(recipe);
          localStorage.setItem('favorites', JSON.stringify(recipeFavorites));
          notify('Successfuly add to favorite', 'success', 3000);
        }
      } else {
        recipeFavorites.push(recipe);
        localStorage.setItem('favorites', JSON.stringify(recipeFavorites));
        notify('Successfuly add to favorite', 'success', 3000);
      }
    } catch (error) {
      throw error;
    }
  },
  removeFromFavorites(recipe: any): void {
    try {
      let recipeFavorites = [];
      if (localStorage.getItem('favorites')) {
        recipeFavorites = JSON.parse(localStorage.getItem('favorites'));
        for (let i = 0; i < recipeFavorites.length; i++)
          if (recipeFavorites[i].id === recipe.id) {
            recipeFavorites.splice(i, 1);
            localStorage.setItem('favorites', JSON.stringify(recipeFavorites));
            notify('Successfuly removed from favorite', 'success', 3000);
            break;
          }
      }
    } catch (error) {
      throw error;
    }
  },
  getFromFavorites(): any[] {
    try {
      let recipeFavorites = [];
      if (localStorage.getItem('favorites')) {
        recipeFavorites = JSON.parse(localStorage.getItem('favorites'));
      }
      return recipeFavorites;
    } catch (error) {
      throw error;
    }
  },
  checkRecipeExistById(recipe): boolean {
    try {
      let recipeFavorites = [];
      if (localStorage.getItem('favorites')) {
        recipeFavorites = JSON.parse(localStorage.getItem('favorites'));
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < recipeFavorites.length; i++)
          if (recipeFavorites[i].id === recipe.id) {
            return true;
          }
      }
      return false;
    } catch (error) {
      throw error;
    }
  },
};

import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  @Input() public title: string = '';
  public headerBreadCrumb: any[];
  public toolbarContent = [
    {
      widget: 'dxButton',
      location: 'before',
      options: {
        icon: 'menu',
        onClick: () => {
          this.menuToggle.emit();
        },
      },
    },
    {
      location: 'before',
      template: 'positionDataTemplate',
    },
  ];

  @Output()
  public menuToggle = new EventEmitter<boolean>();

  @Input()
  public menuToggleEnabled = false;

  constructor() {
    //
  }
}

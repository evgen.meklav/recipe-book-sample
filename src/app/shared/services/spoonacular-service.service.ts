import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class SpoonacularServiceService {
  private API_URL: string = environment.apiUrl;

  constructor(private httpClient: HttpClient) {}

  public getSearch() {
    return this.httpClient.get(`${this.API_URL}/recipes/search`);
  }

  public getComplexSearch(params: HttpParams) {
    return this.httpClient.get(`${this.API_URL}/recipes/complexSearch`, {
      params,
    });
  }

  public getAutoCompleteRecipeSearch(params: HttpParams) {
    return this.httpClient.get(`${this.API_URL}/recipes/autocomplete`, {
      params,
    });
  }

  public getDetailRecipeInfo(id: number) {
    return this.httpClient.get(
      `${this.API_URL}/recipes/${id}/information?includeNutrition=false`
    );
  }

  public getSimilarRecipe(id: number) {
    return this.httpClient.get(`${this.API_URL}/recipes/${id}/similar`);
  }
}

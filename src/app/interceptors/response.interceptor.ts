import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import notify from 'devextreme/ui/notify';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
  constructor() {
    // Empty const
  }

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap(
        (event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            // do stuff with response if you want
          }
        },
        (err: any) => {
          if (err instanceof HttpErrorResponse) {
            this.displayAlert(err);
          }
        }
      )
    );
  }

  public displayAlert(err) {
    let responseMessage = '';
    if (err.status === 0) {
      responseMessage = 'Povezava s strežnikom je prekinjena.';
    } else {
      responseMessage = err.message;
    }
    // todo: implement some alert message

    notify(responseMessage, 'error');
    console.error(responseMessage);
  }
}

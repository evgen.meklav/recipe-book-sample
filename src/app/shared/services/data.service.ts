import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  public navigationPage = new BehaviorSubject(null);
  public navigationPage$ = this.navigationPage.asObservable();

  public recipeDataShare = new BehaviorSubject(null);
  public recipeDataShare$ = this.recipeDataShare.asObservable();

  constructor() {
    //
  }

  public nextNavigationPage(navigationPage: string) {
    this.navigationPage.next(navigationPage);
  }

  public setRecipeDataShare(recipeDataShare: any) {
    this.recipeDataShare.next(recipeDataShare);
  }
}

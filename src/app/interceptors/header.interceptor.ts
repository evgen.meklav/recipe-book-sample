import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {
  constructor() {
    //
  }

  public intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    request = request.clone({
      params: request.params.set('apiKey', '45c69fd256854ec8bad7b934bd96e20f'),
    });

    request = request.clone({
      headers: request.headers.set('Content-Type', 'application/json'),
    });

    return next.handle(request);
  }
}

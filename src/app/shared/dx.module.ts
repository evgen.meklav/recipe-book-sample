import {
    DxAutocompleteModule, DxBoxModule, DxButtonModule, DxChartModule, DxDataGridModule,
    DxDateBoxModule, DxFormModule, DxNumberBoxModule, DxPopupModule, DxResponsiveBoxModule,
    DxScrollViewModule
} from 'devextreme-angular';

import { NgModule } from '@angular/core';

@NgModule({
  imports: [
    DxDataGridModule,
    DxDateBoxModule,
    DxNumberBoxModule,
    DxButtonModule,
    DxChartModule,
    DxBoxModule,
    DxFormModule,
    DxPopupModule,
    DxScrollViewModule,
    DxResponsiveBoxModule,
    DxAutocompleteModule,
  ],
  exports: [
    DxDataGridModule,
    DxDateBoxModule,
    DxNumberBoxModule,
    DxButtonModule,
    DxChartModule,
    DxBoxModule,
    DxFormModule,
    DxPopupModule,
    DxScrollViewModule,
    DxResponsiveBoxModule,
    DxAutocompleteModule,
  ],
})
export class DxModule {}
